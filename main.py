import os
import telebot
from telebot import types
from datetime import date

bot = telebot.TeleBot(os.environ['TELEGRAM_TOKEN'])
admin_chat_ids = [
    int(i)
    for i in os.environ['ADMIN_CHAT_IDS'].split()
]

user_data = {}
commands = {  
    'start'       : 'запуск бота',
    'help'        : 'информация о командах',
    'new'    : 'новая заметка',
    }
buttons = ('Название','Автор', 'Дата', "Официальная информация", "Отзыв", "restart", "Отправить итоговый вариант" )
fields = ('title','name', 'date', 'info', 'feedback')
step_numbers = range (1, 6)
button_step = dict(zip(buttons, step_numbers))
button_field = dict(zip(buttons, fields))
fieldSelect = types.ReplyKeyboardMarkup(one_time_keyboard=True)  
fieldSelect.add(*buttons)
hideBoard = types.ReplyKeyboardRemove()
#@admin_chat_ids = [974072149]


def get_step(message):
    print(f"get_step: {user_data.get(message.chat.id, {})}")
    return user_data.get(message.chat.id, {}).get('step')


def is_title_step(message):
    return get_step(message) == 1


def is_name_step(message):
    return get_step(message) == 2


def is_date_step(message):
    return get_step(message) == 3


def is_info_step(message):
    return get_step(message) == 4


def is_feedback_step(message):
    return get_step(message) == 5


def is_final_step(message):
    return get_step(message) == 6


def is_send_step(message):
    return get_step(message)


def chat_is_known(message):
    return message.chat.id in user_data


@bot.message_handler(commands=['start'])
def command_start(message):
    if not chat_is_known(message):
        user_data[message.chat.id] = {}
    bot.send_message(
        message.chat.id,
        ("Для начала работы нажмите /new," 
         "если нужна помощь, то /help"))


@bot.message_handler(commands=['help'], func=chat_is_known)
def command_help(m):
    cid = m.chat.id
    help_text = "Команды этого бота: \n"
    for key in commands:  
        help_text += "/" + key + ": "
        help_text += commands[key] + "\n"
    bot.send_message(cid, help_text) 

@bot.message_handler(commands=['new'], func=chat_is_known)
def command_new(message):
    cid = message.chat.id
    user_data[message.chat.id]['note'] = {}
    user_data[cid]['step'] = 1
    bot.send_message(cid, "Название мероприятия")
    print(f"user_data[{cid}]: {user_data[cid]}")
    

@bot.message_handler(func=is_title_step )
def get_title(message):
    cid = message.chat.id
    user_data[cid]['note']['title'] = message.text
    user_data[cid]['step'] = 2
    bot.send_message(cid, "Автор заметки")
    print(f"user_data[{cid}]: {user_data[cid]}")


@bot.message_handler(func=is_name_step )
def get_name(message):
    cid = message.chat.id
    user_data[cid]['note']['name'] = message.text
    user_data[cid]['step'] = 3
    bot.send_message(cid, "Дата события")
    print(f"user_data[{cid}]: {user_data[cid]}")



@bot.message_handler(func=is_date_step)
def get_date(message):
    cid = message.chat.id
    parts = message.text.strip().split('.')
    if len(parts) != 3:
        bot.send_message(cid, "формат даты ДД.ММ.ГГГГ")
        return
    try:
        day = int(parts[0])
        month = int(parts[1])
        year = int(parts[2])
        date(day=day, month=month, year=year)
    except ValueError:
        bot.send_message(cid, "формат даты ДД.ММ.ГГГГ")
        return
    bot.send_message(cid, "Официальная информация о мероприятии")
    user_data[cid]["note"]['date'] = message.text
    user_data[cid]['step'] = 4    
    print(f"user_data[{cid}]: {user_data[cid]}")
    

@bot.message_handler(func=is_info_step)
def get_info(message):
    cid = message.chat.id
    user_data[cid]["note"]['info'] = message.text
    user_data[cid]['step'] = 5    
    bot.send_message(cid, "Ваш отзыв о событии")
    print(f"user_data[{cid}]: {user_data[cid]}")


@bot.message_handler(func=is_feedback_step)
def get_feedback(message):
    cid = message.chat.id
    user_data[cid]["note"]['feedback'] = message.text
    user_data[cid]['step'] = 6    
    bot.send_message(cid, "Итоговая заметка:")
    bot.send_message(cid, format_note(cid), parse_mode="Markdown")
    bot.send_message(cid, "Хотите что-нибудь изменить?", reply_markup=fieldSelect)
    
    print(f"user_data[{cid}]: {user_data[cid]}")
    
def format_note(cid):
     return (
        f'*{user_data[cid]["note"]["title"]}*\n'
        f'{user_data[cid]["note"]["name"]}\n'
        f'_{user_data[cid]["note"]["date"]}_\n'
        f'{user_data[cid]["note"]["info"]}\n'
        f'{user_data[cid]["note"]["feedback"]}\n')
    


@bot.message_handler(func=is_final_step)
def get_final(message):
    cid = message.chat.id
    button = message.text.strip()
    
    if button == 'restart':
        user_data[cid].clear()
        command_new(message)
        return
    
    if button == buttons[-1]:
        for chat_id in admin_chat_ids:
            bot.send_message(cid, "Итоговая заметка:")
            bot.send_message(cid, format_note(cid), parse_mode="Markdown")
        command_start(message)
        return

    user_data[cid]['step'] = button_step[button]
    field = button_field.get(button)
    print(f"field: {field}")
    print(f"button:{button}")
    # тут требуется специальная обработка для граничного условия, потому что в программе прописан сразу следующий шаг в теле функции.
    # происходит накладка, а сообщенние с просьбой написать название мероприятия, прописано в /new, а не отдельно
    # 
    #TODO:написать развернутый комментарий почему нам надо именно так сделать (см запись)
    if button == buttons[0]:
        command_new(message)
        return
    

    if field is None:
        bot.send_message(cid, 'нет такого варианта')
        return


if __name__ == '__main__':
    print(f"button_field: {button_field}")
    print(f"button_step: {button_step}")
    bot.infinity_polling()
