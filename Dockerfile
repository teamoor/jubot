FROM python:3.11


RUN pip install --progress-bar off pytelegrambotapi
COPY main.py .

CMD python main.py

ENV TELEGRAM_TOKEN=
ENV ADMIN_CHAT_IDS=
